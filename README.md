# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* BasicChat, a whatsapp inspired android chat app
* Version 0.1

### How do I get set up? ###
* Open Project folder with Android Studio, gradle should get all dependencies
* build and run normally

### Who do I talk to? ###

* Sebastian Scholle (repo owner)

### Using BasicChat ###
#### Main Activity ####
* Login or register user
* Existing users:
*  user1/test
*  user2/test
* register as many users as you like, see them in the Contacts Activty

#### Contacts Activity ####
* See all registered users (except your active user)
* Select a Contact to start a chat with (opens Chat Activity)

#### Chat Activity ####
* List previous messages in relation to that Contact and your active User
* Send new Messages to that contact
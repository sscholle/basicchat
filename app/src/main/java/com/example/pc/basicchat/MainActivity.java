package com.example.pc.basicchat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

/**
 * Allow the User to Login or Register with entered details
 */
public class MainActivity extends ActionBarActivity {
    public static final String TAG = "MainActivity";
    public static final String PREF_USERNAME = "username";
    public static final String PREF_PASSWORD = "password";

    EditText etUsername;
    EditText etPassword;
    SharedPreferences sharedPref;
    String username;
    String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        defaultBehaviour();
    }

    private void defaultBehaviour(){
        Parse.initialize(this);
        // get stored user details
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String username = sharedPref.getString(PREF_USERNAME, "");
        String password = sharedPref.getString(PREF_PASSWORD, "");
        etUsername = (EditText) findViewById(R.id.username);
        etPassword = (EditText) findViewById(R.id.password);
        // prepopulate text fields
        if (!username.equals("") && !password.equals("")) {
            etUsername.setText(username);
            etPassword.setText(password);
        }
    }

    public void onButtonClick(View v){
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();

        this.username = username;
        this.password = password;

        switch (v.getId()){
            case R.id.login_button:
                if (!username.equals("") && !password.equals("")) {
                    authenticate(username, password);
                }else {
                    showToast("Blank fields not accepted");
                }
                break;

            case R.id.register_button:
                if (!username.equals("") && !password.equals("")) {
                    register(username, password);
                }else {
                    showToast("Blank fields not accepted");
                }
                break;
        }
    }

    /**
     * Create new User and Login
     * An AuthenticationManager would be preferable and useable in all activities
     * however am trying to keep this as 'Proof of Concept' as possible until repeating patterns emerge
     * @param username username
     * @param password password
     */
    public void register(String username, String password){
        ParseUser user = new ParseUser();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(username+"@example.com"); // make unique email
        user.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    ParseUser user = ParseUser.getCurrentUser();
                    Log.v(TAG, "signUpInBackground->done with userId: " + user.getObjectId());
                    didAuthenticate();
                } else {
                    didNotAuthenticate("Cannot Register user (" + e.getMessage() + ")");
                }
            }
        });
    }

    /**
     * Login
     * @param username the users username
     * @param password plain text password
     */
    public void authenticate(String username, String password){
        Log.v(TAG, "Login with " + username + " " + password);
        if(hasConnection()){
            ParseUser.logInInBackground(username, password, new LogInCallback() {
                public void done(ParseUser user, ParseException e) {
                    if (user != null) {
                        Log.v(TAG, "loginInBackground->done with userId: " + user.getObjectId());
                        didAuthenticate();
                    } else {
                        didNotAuthenticate("Cannot Authorise user (" + e.getMessage() + ")");
                    }
                }
            });
        }else{
            showToast("Check your Internet connection");
        }
    }

    /**
     * Did Authenticate successfully
     */
    public void didAuthenticate(){
        Log.v(TAG, "didAuthenticate");
        SharedPreferences.Editor edit = sharedPref.edit();
        edit.putString(PREF_USERNAME, username);
        edit.putString(PREF_PASSWORD, password);
        edit.apply();
        Intent intent = new Intent(this, ContactsActivity.class);
        startActivity(intent);
    }

    /**
     * Did Not Authenticate successfully
     * @param message the error message that is returned by the auth function
     */
    public void didNotAuthenticate(String message){
        Log.v(TAG, "didNotAuthenticate: " + message);
        showToast("Invalid username or password");
    }


    /**
     * Utility function - belongs in an AuthenticationManager
     * @return true if internet is connected
     */
    public boolean hasConnection(){
        ConnectivityManager connMgr = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public void showToast(String message){
        Toast t = Toast.makeText(this, message, Toast.LENGTH_LONG);
        t.show();
    }
}

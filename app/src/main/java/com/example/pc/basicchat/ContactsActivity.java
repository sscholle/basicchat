package com.example.pc.basicchat;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.example.pc.basicchat.adapter.UsersAdapter;
import com.example.pc.basicchat.model.User;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * List Contacts that you can Chat to in a ListView
 */
public class ContactsActivity extends ListActivity {
    public static final String TAG = "ContactsActivity";
    public static final String INTENT_FROM_USER_ID = "fromUserId";
    public static final String INTENT_TO_USER_ID = "toUserId";

    ArrayList<User> arrayOfUsers = new ArrayList<User>();
    UsersAdapter adapter;
    ParseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        defaultBehaviour();
    }

    public void defaultBehaviour(){
        // todo: call super that does generic checking such as authentication status and session token expiry
        user = ParseUser.getCurrentUser();

        adapter = new UsersAdapter(this, arrayOfUsers);
        setListAdapter(adapter);

        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereNotEqualTo("objectId", user.getObjectId()); // exclude current user
        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> objects, ParseException e) {
                if (e == null) {
                    // The query was successful.
                    for (ParseUser user : objects) {
                        arrayOfUsers.add(new User(user.getObjectId(), user.getUsername(), user.getEmail()));
                    }
                    adapter.notifyDataSetChanged();
                } else {
                    // Something went wrong.
                }
            }
        });
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        User u = (User)getListView().getItemAtPosition(position);
        Log.v(TAG, "User: "+u.name);
        // launch chat activity
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(INTENT_FROM_USER_ID, ParseUser.getCurrentUser().getObjectId());
        intent.putExtra(INTENT_TO_USER_ID, u.id);
        startActivity(intent);
    }
}

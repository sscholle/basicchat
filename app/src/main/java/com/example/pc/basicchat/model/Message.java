package com.example.pc.basicchat.model;

import java.util.Date;

/**
 * Created by PC on 2016/05/15.
 */
public class Message {
    public String id;
    public String text;
    public Date date;
    public String fromUserId;
    public String toUserId;

    /**
     *
     * @param id
     * @param text
     * @param date
     * @param fromUserId
     * @param toUserId
     */
    public Message(String id, String text, Date date, String fromUserId, String toUserId) {
        this.id = id;
        this.text = text;
        this.date = date;
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
    }
}

package com.example.pc.basicchat;

import android.app.ListActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.pc.basicchat.adapter.MessagesAdapter;
import com.example.pc.basicchat.adapter.UsersAdapter;
import com.example.pc.basicchat.model.Message;
import com.example.pc.basicchat.model.User;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * List Messages that were sent between the Contact and Yourself
 */
public class ChatActivity extends ListActivity {
    public static final String TAG = "ChatActivity";
    ArrayList<Message> arrayList = new ArrayList<Message>();
    MessagesAdapter adapter;
    String toUserId;
    String fromUserId;
    ListView listView;
    EditText et;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_chat);
        defaultBehaviour();
    }

    public void defaultBehaviour(){
        // todo: call super that does generic checking such as authentication status and session token expiry
        listView = getListView();
        et = (EditText) findViewById(R.id.chat_input);
        et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                listView.smoothScrollToPosition(arrayList.size());
            }
        });
        toUserId = getIntent().getStringExtra(ContactsActivity.INTENT_TO_USER_ID);
        fromUserId = getIntent().getStringExtra(ContactsActivity.INTENT_FROM_USER_ID);

        adapter = new MessagesAdapter(this, arrayList, fromUserId);
        setListAdapter(adapter);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Messages");
        String[] ids = {fromUserId, toUserId};
        query.whereContainedIn("toUserId", Arrays.asList(ids));
        query.whereContainedIn("fromUserId", Arrays.asList(ids));
        query.orderByAscending("createdAt");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    // The query was successful.
                    for (ParseObject item : objects) {
                        arrayList.add(new Message(item.getObjectId(), item.getString("text"), item.getCreatedAt(), item.getString("fromUserId"), item.getString("toUserId")));
                    }
                    adapter.notifyDataSetChanged();
                    listView.smoothScrollToPosition(arrayList.size());
                } else {
                    // Something went wrong.
                }
            }
        });

    }

    /**
     * Note that the receiving user will only get the message when his view is refreshed, so no sockets/polling implemented
     * @param v
     */
    public void sendChat(View v) {
        final EditText et = (EditText) findViewById(R.id.chat_input);
        final Button sb = (Button) findViewById(R.id.send_button);
        sb.setEnabled(false); // prevent multiple sends
        // parse
        final ParseObject message = new ParseObject("Messages");
        message.put("text", et.getText().toString());
        message.put("toUserId", toUserId);
        message.put("fromUserId", fromUserId);
        message.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e == null) {
                    arrayList.add(new Message(message.getObjectId(), message.getString("text"), message.getCreatedAt(), message.getString("fromUserId"), message.getString("toUserId")));
                    adapter.notifyDataSetChanged();
                    et.setText("");
                    listView.smoothScrollToPosition(arrayList.size());

                } else {
                    Log.e(TAG, e.getMessage());
                }
                sb.setEnabled(true);
            }
        });
    }
}
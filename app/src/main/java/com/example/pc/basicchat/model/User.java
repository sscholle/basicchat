package com.example.pc.basicchat.model;

import com.parse.ParseObject;

import org.json.JSONException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by PC on 2016/05/15.
 */
public class User {
    public String id;
    public String name;
    public String email;

    /**
     *
     * @param id
     * @param name
     * @param email
     */
    public User(String id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    /**
     * can do batches
     * @param jsonObjects
     * @return
     */
    public static ArrayList<User> fromJson(List<ParseObject> jsonObjects) {
        ArrayList<User> users = new ArrayList<User>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            ParseObject po = jsonObjects.get(i);
            users.add(new User((String)po.get("id"), (String)po.get("name"), (String)po.get("email")));
        }
        return users;
    }
}

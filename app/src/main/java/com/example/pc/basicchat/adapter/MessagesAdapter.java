package com.example.pc.basicchat.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.pc.basicchat.R;
import com.example.pc.basicchat.model.Message;

import java.util.ArrayList;

/**
 * Created by PC on 2016/05/16.
 */
public class MessagesAdapter extends ArrayAdapter<Message> {
    String comparator;
    public MessagesAdapter(Context context, ArrayList<Message> list, String comparatorRightAlign) {
        super(context, 0, list);
        this.comparator = comparatorRightAlign;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Message item = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            if(item.fromUserId.equals(comparator)){
                // right align
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_message_right, parent, false);
            }else{
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_message, parent, false);
            }
        }else{
            Log.v("MessageAdapter", "reusing old view");
        }
        // Lookup view for data population
        TextView ivMessage = (TextView) convertView.findViewById(R.id.ivText);
        TextView ivExtra = (TextView) convertView.findViewById(R.id.ivExtra);
        // Populate the data into the template view using the data object
        ivMessage.setText(item.text);
        ivExtra.setText(item.date.toString());
        // Return the completed view to render on screen
        return convertView;
    }
}
